chrome.browserAction.onClicked.addListener((tab) => {

    const colorPicker = () => {
        let a = Math.random()
        if(a > 0.5) return 'red';
        else return 'blue';
    }

    chrome.tabs.executeScript({
        code: `document.body.style.backgroundColor="${colorPicker()}"`
    })
})