import App from './App.svelte';
import "libs/dist/bootstrap.js";

// console.log($), called in the index.html

const app = new App({
	target: document.body,
	props: {
		name: 'My Diiogo App'
	}
});

export default app;