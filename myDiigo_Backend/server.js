const express = require("express");
const server = express()

let wasRedirected = false;
server.get("/", (req, res) => {
    wasRedirected = true
    res.redirect("/about/redirected")
})
server.get("/about/:dev", (req, res) => {
    try {
        if (req.params.dev === "true") res.send("Oh pa min - " + typeof req.params.dev)
        else res.send(`MyDiigo Backend -   ${typeof req.params.dev} ${wasRedirected ? "<h3>you were redirectd</h3>" : '<h3>HI!!!</h3>'}`)
        if(wasRedirected) wasRedirected = false
    } catch (err) {
        res.status(404).send("Ocorreu um erro no Backend <br><h3>Erro:</h3>" + err)
    }
})
module.exports = server;
