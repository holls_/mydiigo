<h1 style="color: red; text-align: center"><strong>* My Diigo *</strong></h1>

## Description
The App is a online bookmark repository... 

## Usage
1. ``` cd myDiigo ```
2. read the ``` README.md ```

<h1 style="color: red; text-align: center"><strong>* Development Phase *</strong></h1>

## Todos dev ("tickets")
* Clean the global.css
* > Database
    * NoSQL
    * urls, users, settings (but will probably it will be in the users), pastas (que é para onde os urls vão), 
* Pages
    * Start Page ....
    * Login
    * ...
* Backend
    * Using express
    * Use Mongoose
* Frontend 
    * don't need to use popper.js ( diferent from popper ), maybe use tooltip.js.
    > Navbar
    * Add total folders and total url's with icons, user picture and decide if the user picture goes to the hamburguer menu or it's the button to ioen the menu.

### "Anotations"
Scrollbar done and styled, change it to the sidebar when finish the sidebar

## Todos general
* try to use ``` Agile ```
* Git this shit
* State of Art
* Functionalities
* Make a PWA, does it make sense????
* Use Svelte [X]
* Use sass (It will have to be configure in the rollup.config.js i think)
* Decide on what to use for production
* > Try to make a chrome extension for this *(1)*
* Electron App **It would be fucking great xD** 
* Since it 's a solo project, i 'll have to do **devOps**. I 'll develop it, and then "send" it to production. But what about **secDevOps**?
* > Write the code in typeScript *(2)*

### "Index"
> *(1)* - The idea is to make a chrome extension with a "thing" in the bookmarks that when you drag an __url__ there, opens a litle modal where you can choose the directory where you weant to put the url. 
> *(2)* - Probably have to configure this in the ``` rollup.config.js ```, or add ``` <script type="typescript"> ``` or something like this. 

Notes: 
* > npm login page rules

### Side Notes: 
#### jQuery npm doc
> diferent syntax for babel and for browserify/webpack
```
Babel
Babel is a next generation JavaScript compiler. One of the features is the ability to use ES6/ES2015 modules now, even though browsers do not yet support this feature natively.

import $ from "jquery";

Browserify/Webpack
There are several ways to use Browserify and Webpack. For more information on using these tools, please refer to the corresponding project's documention. In the script, including jQuery will usually look like this...

var $ = require("jquery");
```